package pl.sdacademy;

import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

public class Guitar {
    @Size(min = 3, message = "Pole musi mieć minimum 3 znaki!")
    private String manufacturer;
    @Positive(message = "Pole musi być większe od 0!")
    private int stringCount;

    public String getManufacturer() {
        return manufacturer;
    }

    public int getStringCount() {
        return stringCount;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public void setStringCount(int stringCount) {
        this.stringCount = stringCount;
    }

    @Override
    public String toString() {
        return "Guitar{" +
                "manufacturer='" + manufacturer + '\'' +
                ", stringCount=" + stringCount +
                '}';
    }
}
