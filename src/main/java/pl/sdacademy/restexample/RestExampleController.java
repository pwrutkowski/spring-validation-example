package pl.sdacademy.restexample;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import pl.sdacademy.Guitar;

@RestController
public class RestExampleController {
    private static final Logger LOGGER = LoggerFactory.getLogger(RestExampleController.class);

    @PostMapping("/example1")
    public void example1(@RequestBody @Validated Guitar guitar) {
        LOGGER.info(guitar.toString());
    }
}
