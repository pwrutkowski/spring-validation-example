package pl.sdacademy.thymeleafexample;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import pl.sdacademy.Guitar;

@Controller
public class ThymeleafExampleController {
    @GetMapping("/th-example")
    public String exampleGet(ModelMap modelMap) {
        modelMap.addAttribute("guitar", new Guitar());
        return "example-form";
    }

    @PostMapping("/th-example")
    public String examplePost(ModelMap modelMap, @Validated Guitar guitar, BindingResult bindingResult) {
        modelMap.addAttribute("guitar", guitar);
        if (bindingResult.hasErrors()) {
            return "example-form";
        } else {
            return "example-result";
        }
    }

}
